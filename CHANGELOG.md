# Changelog

## Unreleased

### fixed
- appeler le logo svg dans paquet.xml
- refacto du logo svg et case minuscule du -xx dans le nom du fichier

## 1.0.0 - 2023-06-15

### Added

- ajout d'un CHANGELOG.md

### Changed

- compatibilité SPIP 4.2
- Compatibilité SPIP 4.1 mini
- Logos du plugin en SVG
- utiliser le plugin Fontawesome à la place de la police Fontawesome embarquée avec le thème